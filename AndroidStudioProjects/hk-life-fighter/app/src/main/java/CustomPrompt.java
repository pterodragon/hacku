import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.hk_life_fighter.hk_life_fighter.R;

/**
 * Created by wing on 4/16/16.
 */
public class CustomPrompt extends LinearLayout{
    public CustomPrompt(Context context) {
        super(context);
         LayoutInflater layoutInflater = LayoutInflater.from(context);
        layoutInflater.inflate(R.layout.prompts, this);
    }
}
