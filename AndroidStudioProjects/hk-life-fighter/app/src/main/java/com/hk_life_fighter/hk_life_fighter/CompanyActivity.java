package com.hk_life_fighter.hk_life_fighter;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by kcchanal on 4/17/16.
 */
public class CompanyActivity extends AppCompatActivity implements ImageButton.OnTouchListener {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    static int energyLevel = 5;
    static int money = 2000;
    static int skillpoint = 5;
    static int timer = 0;

    static int NumButton = 6;
    private int screenWidth = 0;
    private int screenHeight = 0;

    private ImageButton careerButton[];
    private Bitmap careerImageClicked[];
    private Bitmap careerImage[];

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideNavigationBar();
        setContentView(R.layout.company_activity);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

//        getScreenDimension();
        prepareButtons();
        prepareImages();
    }

    private void prepareButtons() {
        careerButton = new ImageButton[NumButton];
        int[] viewId = {R.id.Clerkbutton, R.id.Servicebutton, R.id.Lawyerbutton, R.id.Govbutton, R.id.Enginebutton,
                R.id.Finbutton};
        for (int i = 0; i < NumButton; i++) {
            careerButton[i] = (ImageButton) findViewById(viewId[i]);
            careerButton[i].setOnTouchListener(this);
        }

        final AppCompatActivity a = this;
        ImageView iv = (ImageView) findViewById(R.id.companybackbutton);
        iv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                a.finish();
                return true;
            }
        });
    }

    private void prepareImages() {
//        int[] drawId = 
//        companyImageClicked = BitmapFactory.decodeResource(getResources(), R.drawable.company20);
//        companyImage = BitmapFactory.decodeResource(getResources(), R.drawable.company18);
//        schoolImage = BitmapFactory.decodeResource(getResources(), R.drawable.school18);
//        schoolImageClicked = BitmapFactory.decodeResource(getResources(), R.drawable.school20);
//        schoolImage = BitmapFactory.decodeResource(getResources(), R.drawable.school18);
//        schoolImageClicked = BitmapFactory.decodeResource(getResources(), R.drawable.school20);
//        house2Image = BitmapFactory.decodeResource(getResources(), R.drawable.house2_18);
//        house2ImageClicked = BitmapFactory.decodeResource(getResources(), R.drawable.house2_20);
//        houseImage = BitmapFactory.decodeResource(getResources(), R.drawable.house18);
//        houseImageClicked = BitmapFactory.decodeResource(getResources(), R.drawable.house20);
    }

    private void hideNavigationBar() {
        //TODO: 16-4-2016: Hide Navigation Bar
        //Remove title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                //| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_IMMERSIVE
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        final View decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        // Note that system bars will only be "visible" if none of the
                        // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
                        if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                            // TODO: The system bars are visible. Make any desired
                            // adjustments to your UI, such as showing the action bar or
                            // other navigational controls.

                            //decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                            //decorView.setSystemUiVisibility(decorView.getSystemUiVisibility());
                            //getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
                            decorView.setSystemUiVisibility(flags);
                            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
                        } else {
                            // TODO: The system bars are NOT visible. Make any desired
                            // adjustments to your UI, such as hiding the action bar or
                            // other navigational controls.
                        }
                    }
                });
        decorView.setSystemUiVisibility(flags);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            switch (v.getId()) {
                case R.id.Clerkbutton:
                    Log.v("ClerkButton", "ClerkButtonnnnnnn down down down!");
//                    careerButton[0].setImageBitmap(careerImageClicked[0]);
                    break;
                case R.id.Servicebutton:
//                    careerButton[1].setImageBitmap(careerImageClicked[1]);
                    break;
                case R.id.Lawyerbutton:
//                    careerButton[2].setImageBitmap(careerImageClicked[2]);
                    break;
                case R.id.Govbutton:
//                    careerButton[3].setImageBitmap(careerImageClicked[3]);
                    break;
                case R.id.Enginebutton:
//                    careerButton[4].setImageBitmap(careerImageClicked[4]);
                    break;
                case R.id.Finbutton:
//                    careerButton[5].setImageBitmap(careerImageClicked[5]);
                    break;
                default:
                    break;
            }
            return true;
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            switch (v.getId()) {
                case R.id.Clerkbutton:
                    Log.v("ClerkButton", "ClerkButtonnnnnnn   upupupupu!");

//                    careerButton[0].setImageBitmap(careerImage[0]);
                    Intent intent = new Intent(getApplicationContext(), JobActivity.class);
                    startActivity(intent);
                    break;
                case R.id.Servicebutton:
                    createAndShowAlertDialog("Title", false);
//                    careerButton[1].setImageBitmap(careerImage[1]);
                    break;
                case R.id.Lawyerbutton:
//                    if()
                    careerButton[2].getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
//                    else
//                        button.getBackground().setColorFilter(null);
//                    careerButton[2].setImageBitmap(careerImage[2]);
                    break;
                case R.id.Govbutton:
//                    careerButton[3].setImageBitmap(careerImage[3]);
                    break;
                case R.id.Enginebutton:
//                    careerButton[4].setImageBitmap(careerImage[4]);
                    break;
                case R.id.Finbutton:
//                    careerButton[5].setImageBitmap(careerImage[5]);
                    break;
                default:
                    break;
            }
            return true;
        }
        return true;
    }

    private void createAndShowAlertDialog(String title, boolean isOneButton) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        final boolean isTrue;
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                //TODO
//                setTrue(1);
                Log.v("Prompt DiaLogue", "sadsadsadsad gogogogogog   true");
                dialog.dismiss();

            }
        });
        if (!isOneButton) {
            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    //TODO
//                    setTrue(0);
                    Log.v("Prompt DiaLogue", "sadsadsadsad gogogogogog   false");

                    dialog.dismiss();
                }
            });
        }
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}