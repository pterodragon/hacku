package com.hk_life_fighter.hk_life_fighter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements ImageButton.OnTouchListener {

    static int energyLevel = 5;
    static int money = 2000;
    static int skillpoint = 5;
    static int timer = 0;

    private int screenWidth = 0;
    private int screenHeight = 0;
    private ImageButton schoolButton;
    private ImageButton companyButton;
    private ImageButton house2Button;
    private ImageButton houseButton;

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onResume() {
        updateMoneySkillP();
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideNavigationBar();
        showWelcomeScreen();
        getScreenDimension();
        prepareImages();
    }

    private Bitmap schoolImageClicked;
    private Bitmap schoolImage;
    private Bitmap companyImageClicked;
    private Bitmap companyImage;
    private Bitmap houseImageClicked;
    private Bitmap houseImage;
    private Bitmap house2ImageClicked;
    private Bitmap house2Image;


    private void updateMoneySkillP() {
        Log.e("Money!!!", "" + (((TextView) findViewById(R.id.mapTextView_money_text)) == null));
        TextView tvS = (TextView) findViewById(R.id.mapTextView_skillsbar_text);
        if (tvS != null) tvS.setText("" + ModelData.skillpoint);
        TextView tvM = (TextView) findViewById(R.id.mapTextView_money_text);
        if (tvM != null) tvM.setText("" + ModelData.money);
    }

    private void prepareButtons() {
        schoolButton = (ImageButton) findViewById(R.id.schoolButton);
        companyButton = (ImageButton) findViewById(R.id.companyButton);
        house2Button = (ImageButton) findViewById(R.id.house2Button);
        houseButton = (ImageButton) findViewById(R.id.houseButton);
        schoolButton.setOnTouchListener(this);
        companyButton.setOnTouchListener(this);
        house2Button.setOnTouchListener(this);
        houseButton.setOnTouchListener(this);
    }

    private void prepareImages() {
        companyImageClicked = BitmapFactory.decodeResource(getResources(), R.drawable.company20);
        companyImage = BitmapFactory.decodeResource(getResources(), R.drawable.company18);
        schoolImage = BitmapFactory.decodeResource(getResources(), R.drawable.school18);
        schoolImageClicked = BitmapFactory.decodeResource(getResources(), R.drawable.school20);
        schoolImage = BitmapFactory.decodeResource(getResources(), R.drawable.school18);
        schoolImageClicked = BitmapFactory.decodeResource(getResources(), R.drawable.school20);
        house2Image = BitmapFactory.decodeResource(getResources(), R.drawable.house2_18);
        house2ImageClicked = BitmapFactory.decodeResource(getResources(), R.drawable.house2_20);
        houseImage = ModelData.isBuyHouse ? BitmapFactory.decodeResource(getResources(), R.drawable.house3_18_)
                : BitmapFactory.decodeResource(getResources(), R.drawable.house18);
        houseImageClicked = ModelData.isBuyHouse ? BitmapFactory.decodeResource(getResources(), R.drawable.house3_20_)
                : BitmapFactory.decodeResource(getResources(), R.drawable.house20);
    }

    private void showWelcomeScreen() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.welcome_screen, null);
        final MainActivity m = this;
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setContentView(R.layout.activity_main);
                prepareButtons();
                updateMoneySkillP();

                final TextView tv = (TextView) m.findViewById(R.id.mapTextView_timer_text);
                new CountDownTimer(1800000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        String res = String.format("%02d:%02d", millisUntilFinished / 1000 / 60, millisUntilFinished / 1000 % 60);
                        tv.setText(res);
                        //here you can have your logic to set text to edittext
                    }

                    public void onFinish() {
                        tv.setText("");
                    }

                }.start();
            }


        });
        setContentView(view);


    }

    private void hideNavigationBar() {
        //TODO: 16-4-2016: Hide Navigation Bar
        //Remove title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                //| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_IMMERSIVE
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        final View decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        // Note that system bars will only be "visible" if none of the
                        // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
                        if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                            // TODO: The system bars are visible. Make any desired
                            // adjustments to your UI, such as showing the action bar or
                            // other navigational controls.

                            //decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                            //decorView.setSystemUiVisibility(decorView.getSystemUiVisibility());
                            //getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
                            decorView.setSystemUiVisibility(flags);
                            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
                        } else {
                            // TODO: The system bars are NOT visible. Make any desired
                            // adjustments to your UI, such as hiding the action bar or
                            // other navigational controls.
                        }
                    }
                });
        decorView.setSystemUiVisibility(flags);
    }

    public void getScreenDimension() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screenWidth = size.x;
        screenHeight = size.y;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            switch (v.getId()) {
                case R.id.houseButton:
                    houseButton.setImageBitmap(houseImageClicked);
                    break;
                case R.id.house2Button:
                    house2Button.setImageBitmap(house2ImageClicked);
                    break;
                case R.id.companyButton:
                    companyButton.setImageBitmap(companyImageClicked);
                    break;
                case R.id.schoolButton:
                    schoolButton.setImageBitmap(schoolImageClicked);
                    break;
                default:
                    break;
            }
            return true;
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            switch (v.getId()) {
                case R.id.houseButton:
                    houseButton.setImageBitmap(houseImage);
                    if (money < 1000) {
                        createAndShowAlertDialog("You do not have enough money to buy it!", true, "");
                    } else {
                        createAndShowAlertDialog("Are you sure to buy?", false, "BuyAction");
                    }
                    break;
                case R.id.house2Button:
                    house2Button.setImageBitmap(house2Image);
                    break;
                case R.id.companyButton:
                    Intent intent = new Intent(getApplicationContext(), CompanyActivity.class);
                    startActivity(intent);
                    companyButton.setImageBitmap(companyImage);
                    break;
                case R.id.schoolButton:
                    intent = new Intent(getApplicationContext(), Minigame.class);
                    startActivity(intent);
                    schoolButton.setImageBitmap(schoolImage);
                    break;
                default:
                    break;
            }
            return true;
        }
        return true;
    }

    private void setMoneySkillP(int Money) {
        ((TextView) findViewById(R.id.mapTextView_money_text)).setText("" + money);
    }

    private void createAndShowAlertDialog(String title, boolean isOneButton, final String Action) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(title);
        final boolean isTrue;
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                //TODO
//                setTrue(1);
                if (Action.equals("BuyAction")) {
                    money -= 1000;
                    houseButton.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.house3_18_));
                    houseImageClicked = BitmapFactory.decodeResource(getResources(), R.drawable.house3_20_);
                    houseImage = BitmapFactory.decodeResource(getResources(), R.drawable.house3_18_);
                    setMoneySkillP(money);
                    ModelData.isBuyHouse = true;
                }
                dialog.dismiss();

            }
        });
        if (!isOneButton) {
            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    //TODO
//                    setTrue(0);
                    Log.v("Prompt DiaLogue", "sadsadsadsad gogogogogog   false");

                    dialog.dismiss();
                }
            });
        }
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
