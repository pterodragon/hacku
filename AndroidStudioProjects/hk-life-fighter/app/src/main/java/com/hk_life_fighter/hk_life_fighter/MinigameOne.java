package com.hk_life_fighter.hk_life_fighter;


import android.content.Context;
import android.content.DialogInterface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import java.text.DecimalFormat;

public class MinigameOne extends AppCompatActivity {

    public int damage = 720;
    public boolean flag = false;

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        setContentView(R.layout.minigameone);
        final AppCompatActivity a = this;
        sensorManager.registerListener(new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {

                float x = event.values[0];
                float y = event.values[1];
                float z = event.values[2];
                double g = Math.sqrt(x * x + y * y + z * z);
                TextView gtv = (TextView) findViewById(R.id.minigameOnetvgforce);
                if (damage < 600) {
                    damage--;
                    gtv.setText("Win");
                    Log.d("mydebug", "A");
                } else {
                    DecimalFormat df = new DecimalFormat("#.0");
                    gtv.setText(df.format(g));
                    Log.d("mydebug", "B");
                }
                Log.d("mydebug", String.valueOf(damage));
                if (g > 25) {
                    damage--;
                }
                if (damage < 0 && !flag) {
                    flag = true;
                    sensorManager.unregisterListener(this);
                    TextView tv = (TextView) findViewById(R.id.minigameOnetv);
                    tv.setText("");
                    ModelData.money+=100;
                    ModelData.skillpoint+=5;
                    a.finish();
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
            }

        }, sensor, SensorManager.SENSOR_DELAY_FASTEST);

    }

    private void createAndShowAlertDialog(String title, boolean isOneButton) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        final boolean isTrue;
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                //TOhiDO
//                setTrue(1);
                Log.v("Prompt DiaLogue", "sadsadsadsad gogogogogog   true");
//                functionToDoCall();
                dialog.dismiss();

            }
        });
        if (!isOneButton) {
            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    //TODO
//                    setTrue(0);
                    Log.v("Prompt DiaLogue", "sadsadsadsad gogogogogog   false");

                    dialog.dismiss();
                }
            });
        }
        AlertDialog dialog = builder.create();
        dialog.show();
    }


}
