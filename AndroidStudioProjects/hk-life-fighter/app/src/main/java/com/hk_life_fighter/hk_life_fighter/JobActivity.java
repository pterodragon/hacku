package com.hk_life_fighter.hk_life_fighter;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

public class JobActivity extends AppCompatActivity implements ImageButton.OnTouchListener {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideNavigationBar();
        setContentView(R.layout.job_activity);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        prepareButtons();
    }

    private void prepareButtons() {
        TextView tv1 = (TextView) findViewById(R.id.jobbutton1);
        TextView tv2 = (TextView) findViewById(R.id.jobbutton2);
        TextView tv3 = (TextView) findViewById(R.id.jobbutton3);
        TextView[] tva = {tv1, tv2, tv3};
        if (ModelData.rank == 0) {
            for (TextView tv : tva) {
                tv.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                tv.setOnTouchListener(this);
            }
        }

        // put the back button on the bottom right corner
        final AppCompatActivity a = this;
        ImageView iv = (ImageView) findViewById(R.id.jobactivitybackbutton);
        iv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                a.finish();
                return true;
            }
        });

    }


    private void hideNavigationBar() {
        //TODO: 16-4-2016: Hide Navigation Bar
        //Remove title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                //| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_IMMERSIVE
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        final View decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        // Note that system bars will only be "visible" if none of the
                        // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
                        if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                            // TODO: The system bars are visible. Make any desired
                            // adjustments to your UI, such as showing the action bar or
                            // other navigational controls.

                            //decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                            //decorView.setSystemUiVisibility(decorView.getSystemUiVisibility());
                            //getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
                            decorView.setSystemUiVisibility(flags);
                            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
                        } else {
                            // TODO: The system bars are NOT visible. Make any desired
                            // adjustments to your UI, such as hiding the action bar or
                            // other navigational controls.
                        }
                    }
                });
        decorView.setSystemUiVisibility(flags);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        TextView tv1 = (TextView) findViewById(R.id.jobbutton1);
        TextView tv2 = (TextView) findViewById(R.id.jobbutton2);
        TextView tv3 = (TextView) findViewById(R.id.jobbutton3);
        TextView[] tva = {tv1, tv2, tv3};
        if (event.getAction() == MotionEvent.ACTION_UP) {
            switch (v.getId()) {
                case R.id.jobbutton1:
                    Log.v("ClerkButton1", "ClerkButton up");
                    boolean clerk1BoughtAlready = tv1.getBackground().getColorFilter() == null;
                    if (clerk1BoughtAlready)
                        createAndShowAlertDialog("Already have the career!", true, "", null);
                    else if (!clerk1BoughtAlready && ModelData.skillpoint >= 1) {
                        Log.d("jobactivity", "skillpoint--");
                        createAndShowAlertDialog("Taking the job", true, "Action", tv1);
                    } else {
                        createAndShowAlertDialog("Not enough social points!", true, "", null);
                    }
                    break;
                case R.id.jobbutton2:
//                    Log.v("ClerkButton2", "ClerkButton up");
//                    boolean clerk2BoughtAlready = jobButton[1].getBackground().getColorFilter() == null;
//                    if (clerk2BoughtAlready)
//                        createAndShowAlertDialog("Already have the career!", true, "", null);
//                    else if (!clerk2BoughtAlready && ModelData.skillpoint >= 1) {
//                        Log.d("jobactivity", "skillpoint--");
//
//                        createAndShowAlertDialog("Taking the job", true, "Action", jobButton[1]);
//                    } else {
//                        createAndShowAlertDialog("Not enough social points!", true, "", null);
//                    }
                    break;
                case R.id.jobbutton3:
//                    Log.v("ClerkButton3", "ClerkButton up");
//                    boolean clerk3BoughtAlready = jobButton[2].getBackground().getColorFilter() == null;
//                    if (clerk3BoughtAlready)
//                        createAndShowAlertDialog("Already have the career!", true, "", null);
//                    else if (!clerk3BoughtAlready && ModelData.skillpoint >= 1) {
//                        Log.d("jobactivity", "skillpoint--");
//
//                        createAndShowAlertDialog("Taking the job", true, "Action", jobButton[2]);
//                    } else {
//                        createAndShowAlertDialog("Not enough social points!", true, "", null);
//                    }
                    break;
                default:
                    break;
            }
            return true;
        }
        return true;
    }


    private void createAndShowAlertDialog(String title, boolean isOneButton, final String actionName, final TextView button) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        final boolean isTrue;
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                //TODO
//                setTrue(1);
                Log.v("Prompt DiaLogue", "sadsadsadsad gogogogogog   true");
//                functionToDoCall();
                if (actionName.equals("Action")) {
                    ModelData.skillpoint--;
                    ModelData.money += 1000;
                    ModelData.rank++;
                    button.getBackground().setColorFilter(null);

                }
                dialog.dismiss();

            }
        });
        if (!isOneButton) {
            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    //TODO
//                    setTrue(0);
                    Log.v("Prompt DiaLogue", "sadsadsadsad gogogogogog   false");

                    dialog.dismiss();
                }
            });
        }
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
