## git workflow

`git clone https://bitbucket.org/pterodragon/hacku`
`git fetch`
`git pull` # only if you want to modify the local current branch
`git add` `git commit` etc.
`git push`

Others:
`git checkout -b <branch name>` # create a new branch
`git checkout <branch name>`

